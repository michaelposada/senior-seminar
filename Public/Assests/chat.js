// Make connection
var socket = io.connect(location.origin);


// Query DOM
var message = document.getElementById('message'),
      handle = document.getElementById('handle'),
      list = document.getElementById('myBtn'),
      output = document.getElementById('output'),
      feedback = document.getElementById('feedback'),
      chat = document.getElementById('left');
      //userProfile = document.getElementsById('test');
//***BIG PP***//
//variables we will need for packet construction

//***packet construction**//

//sender packet comType:comId:userSend:timeSend:msg
var senderComType = chat.getAttribute("isgroupchat");
var senderComId = chat.getAttribute("activechatid");
var senderUserSend = handle.getAttribute("myid");

var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var senderTimeSend = date+' '+time;

var senderMsg = message.value;
var friends = "";
var logs = "";
//var friendConvo = document.getElementById('handle');
//otw packet "one time where"//gets the data for which chat you want to use based on the chat that you click

//msg receiver packet//packet for receiving a message

//requester packet//packet for requesting someone to be your friend or join your groupchat

//***end packet construction***//


//***END BIG PP**//

// Emit events

//friendConvo.addEventListener('click', function()
//{
	//alert("TEST");
//});

message.addEventListener('keypress', function(e){

var key = e.which || e.keyCode;

if(key == 13)
{
var testText = message.value
testText = testText.trim()
if(testText == "" ){
//nothing happens no blank spaces
}
else
{
	senderComId = chat.getAttribute("activechatid");
	socket.emit('chat', {
        isGroupChat: senderComType, // Check for Group Chat
	comId: senderComId, //Name of the Reciver
	userSend: senderUserSend, //Name of the Sender
	message: message.value, //Message of the Sender
        handle: senderUserSend, //id Number
	time
    });
    message.value = "";
}
}
});
//Event when user is typing it will say so and so are typing
message.addEventListener('keypress', function(){
	senderComId = chat.getAttribute("activechatid");
	socket.emit('typing', {handle: senderUserSend, comId:senderComId});
	});

// Event when user clicks friends button
list.addEventListener('click', function(){
	socket.emit('friends', { handle: senderUserSend, friends});
});


// To see if we can Populate on Connection
//socket.emit('populate', function(){
//handle: senderUserSend});

socket.on('populate')
{
	//alert("HELLO??")
	socket.emit('fill',{handle: senderUserSend, friends});
	//assignListeners();
};



//List of Events
socket.on('fillInformation', function(data)
{
//	alert("We are doing FillInformation");
	while(chat.hasChildNodes())
                {
                        chat.removeChild(chat.firstChild);
                }
                chat.appendChild(list)
		var i = 1;
                while(data.list.indexOf(',') != -1)
                {
			//alert("IN FILL INFORMATION")
                        var friendName = data.list.split(',')[0];
                        var tempDiv = document.createElement("div");
			chat.setAttribute("numFriends",""+i);
                        var tempButton = document.createElement("button");
                        tempButton.className = "friend"
                        tempDiv.appendChild(tempButton);

			tempButton.setAttribute("id","chatid" + i);

                        tempButton.setAttribute("chatid","" +data.handle);
                        //tempButton.setAtrribute("friendid", "" + friendName);
                        //tempButton.setAtrribute("id", ""+ i);
                        tempButton.innerHTML = "" + friendName;

                        chat.appendChild(tempDiv);
                       
                        data.list = data.list.replace(friendName,"");
                        data.list = data.list.substr(1);
                        friendName = ""
                        i++
                }
assignListeners();

});


//Response For Messages
socket.on('chat', function(data){
    feedback.innerHTML = '';
 //   output.innerHTML += '<p><strong>' + data.userSend + '</strong> <br>' + data.message + '</p>';
    output.innerHTML += '<p><strong>' + data.userSend + '</strong><br><br>'
		      + '<span style="opacity:0.7; font-size:x-small">  @ ' + data.time + '</span><br><br>'
                      + '<span style="border-left: 5px solid #8ab0e2; padding: 5px 0px 5px 10px; font-size:10pt;">' + data.message + '</p>';
});

//Response For Typing
socket.on('typing', function(data){
    	document.getElementById("feedback").style.fontSize = "small";
	document.getElementById("feedback").style.opacity = "0.7";
	feedback.innerHTML = '<p><em>' + data.handle + ' is typing a message...</em></p>';
});

//Response For Friendslist
socket.on('friends', function(data){

		var tempDiv;
		var tempButton;
		while(chat.hasChildNodes())
		{
			chat.removeChild(chat.firstChild);
		}
		chat.appendChild(list)

		while(data.list.indexOf(',') != -1)
		{
			//var i = 1
			var friendName = data.list.split(',')[0];
			tempDiv = document.createElement("div");
			tempButton = document.createElement("button");
			tempButton.className = "friend"
			tempDiv.appendChild(tempButton);

			tempButton.setAttribute("id","" +data.handle);
			//tempButton.setAtrribute("friendid", "" + friendName);
			//tempButton.setAtrribute("id", ""+ i);
		        tempButton.innerHTML = "" + friendName;

			chat.appendChild(tempDiv);
			data.list = data.list.replace(friendName,"");
			data.list = data.list.substr(1);
			friendName = ""

			//i++
		}
});


socket.on("1on1Conversation", function(data) 
{
	feedback.innerHTML = '';
	var boolean = true;
	var i = 0;

	while(output.hasChildNodes())
	{
		output.removeChild(output.firstChild);
	}
	while(boolean == true)
	{
		
		if(data.logs[i] == undefined)
		{
		   boolean = false
		}
		else
		{

			var sender = data.logs[i].usernamesender;
			var msg = data.logs[i].message;
			var timestamp = data.logs[i].timestamp;
			document.getElementById("output").style.maxHeight = "80%";
  			document.getElementById("output").style.overflow = "auto";
			var element = document.getElementById("output");
                        element.scrollTop = element.scrollHeight;
			output.innerHTML += '<p><strong>' + sender + '</strong>' 
				+ '<span style="opacity:0.7; font-size:x-small">  @ ' + timestamp + '</span><br><br>'
				+ '<span style="border-left: 5px solid #8ab0e2; padding: 5px 0px 5px 10px; font-size:10pt;">' + msg + '</p>';
			i++;
		}
	}


});


//socket.on('popUpName', function(data){
	//This does nothing yet
//	var text = test.getElelmentsByTagName("span");
//	text.innerHTML = data.handle
//});

function assignListeners()
{
	for(i = 1; i <= chat.getAttribute("numfriends"); i++)
	{
//	alert(i)

	var testButton  = document.getElementById("chatid" + i);
//	alert(testButton.innerHTML)
//beginning of closure		
(function (testButton) {
			testButton.addEventListener('click',function()
                {
  //                      alert(testButton.innerHTML);
			chat.setAttribute("activechatid", testButton.innerHTML);
			socket.emit('conversation1on1', {
			handle: senderUserSend,
			friend: chat.getAttribute("activechatid"),
			logs});
                });
		})(testButton);
//end of closure


	}
}
