<?php
	if(isset($_POST['user']) and !preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $_POST['user'])){
		$username = $_POST['user'];
		$flag1 = true;
	} else {
		echo "username not set or has invalid characters! <br>";
		$flag1 = false;
	}
	//Check User ID
	if(isset($_POST['pass']) and !preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $_POST['pass'])){
		$password = hash('sha256', $_POST['pass']);
		$flag2 = true;
	} else {
		echo "password not set or has invalid characters! <br>";
		$flag2 = false;
	}
		
	if($flag1 && $flag2){
		//Connect to DB
		session_start();
		try {
			$dbh = new PDO('pgsql:dbname=valley');
		} catch (PDOException $e) {
			print "Error: ".$e->getMessage()."<br/>";
			die();
		}//WILL HAVE TO EDIT THE QUERY
		$st = $dbh->prepare("
	SELECT id
	FROM userpass
	WHERE username=?
	AND password=?
	LIMIT 1");
		$st->bindParam(1, $username);
		$st->bindParam(2, $password);
		$st->execute();
		$result = $st->fetch();
		print($result[0]);
		$succ = false;
		if($result[0] == null){
			$succ = false;
			header('Location: ../index.html');//figure out where we want to go
		}else{
			$succ = true;
			//in case result fails but isnt null (shouldnt occur but i will leave this here as a reminder until we can do some testing
		}
		if($succ){
			session_start();
			$_SESSION['userID'] = $result[0];
			header('Location: ../View/home.php');//figure out where we want to go
		}
	}
?>
